import {Directive, ElementRef, Input, Output} from '@angular/core';
import {Character} from '../classes/characters/character';
import {Linear} from 'gsap/all';


declare var TweenMax: any;


@Directive({
  selector: '[appProgressBarAnimated]'
})
export class ProgressBarAnimatedDirective {

  progressBar: ElementRef;

  @Input() character: Character;

  constructor(elRef: ElementRef) {
    this.progressBar = elRef.nativeElement;
  }

  doIt(): void {
    TweenMax.fromTo(this.progressBar.nativeElement, this.character.attackSpeed / 1000, {width: 0}, {width: 100, ease: Linear.easeNone});
    // TweenMax.fromTo(this.box.nativeElement, 2, {x: 20}, {x: 440, ease: Power1.easeOut});
    // TweenMax.fromTo(this.box.nativeElement, 2, {y: 20}, {y: 440, ease: Bounce.easeOut});
  }

}
