import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


import {AppComponent} from './app.component';
import {BattleComponent} from './components/battle/battle.component';
import {ShopComponent} from './components/shop/shop.component';
import {InventoryComponent} from './components/inventory/inventory.component';
import {SkillwindowComponent} from './components/skillwindow/skillwindow.component';
import {CharacterwindowComponent} from './components/characterwindow/characterwindow.component';
import {MainwindowComponent} from './components/mainwindow/mainwindow.component';
import {HerocreatorComponent} from './components/herocreator/herocreator.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {OverviewComponent} from './components/overview/overview.component';

import {FormsModule} from '@angular/forms';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule
} from '@angular/material';
import {DndModule} from 'ngx-drag-drop';
import { ItemSlotComponent } from './components/characterwindow/equipment-slot/item-slot.component';
import { ProgressBarComponent } from './components/animations/progress-bar/progress-bar.component';
import { BattleHeroComponent } from './components/battle/battle-hero/battle-hero.component';
import { BattleEnemyComponent } from './components/battle/battle-enemy/battle-enemy.component';
import { ProgressBarAnimatedDirective } from './directives/progress-bar-animated.directive';



@NgModule({
  declarations: [
    AppComponent,
    BattleComponent,
    ShopComponent,
    InventoryComponent,
    SkillwindowComponent,
    CharacterwindowComponent,
    MainwindowComponent,
    HerocreatorComponent,
    OverviewComponent,
    ItemSlotComponent,
    ProgressBarComponent,
    BattleHeroComponent,
    BattleEnemyComponent,
    ProgressBarAnimatedDirective],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    DndModule,
  ],
  exports: [
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
