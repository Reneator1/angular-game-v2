import {Injectable} from '@angular/core';
import {Item} from '../classes/items/Item';
import {ITEM_LIST} from '../mockdata/mock-equipment';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor() {
  }

  getItem(itemId: number): Item {
    let itemReturn: Item;
    ITEM_LIST.some(item => {
      if (item.itemId === itemId) {
        itemReturn = item.clone();
        return true;
      }
    });

    return itemReturn;
  }

  getItems(itemIds: number[]): Item[] {
    const items: Item[] = [];
    itemIds.forEach(itemId => {
      items.push(this.getItem(itemId));
    });
    return items;
  }
}
