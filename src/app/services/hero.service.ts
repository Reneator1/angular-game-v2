import {Injectable} from '@angular/core';

import {Hero} from '../classes/characters/hero';
import {HeroClass} from '../classes/characters/hero-class';
import {HeroLevelService} from './herolevel.service';
import {CharacterInventory} from '../classes/characters/charactercontainers/character-inventory';
import {CharacterEquipment} from '../classes/characters/charactercontainers/character-equipment';
import {EquipmentSlotType} from '../classes/items/equipment/equipment-slot.enum';
import {ClassTemplate} from '../classes/characters/class-template';
import {CLASS_TEMPLATES} from '../mockdata/mock-classes-templates';
import {Item} from '../classes/items/Item';
import {ItemService} from './item.service';

@Injectable()
export class HeroService {


  constructor(private heroLevelService: HeroLevelService, private itemService: ItemService) {
  }


  // generates or resets the hero
  generateHero(hero: Hero, name: string, heroClass: HeroClass) {
    // getStatsForClass(heroClass);

    this.buildHeroByTemplate(hero, name, heroClass);
  }

  buildHeroByTemplate(hero: Hero, name: string, heroClass: HeroClass) {

    const template: ClassTemplate = this.getTemplate(heroClass);

    hero.name = name;
    hero.heroClass = heroClass;
    hero.hp = 500;
    hero.damage = 5;
    hero.gold = 0;
    hero.mana = 0;
    hero.alive = true;
    hero.img = template.imgPortrait;
    hero.equipment = this.buildCharacterEquipment(template);
    this.gearCharacterEquipment(template, hero.equipment);
    hero.inventory = new CharacterInventory(template.inventorySize);
    hero.heroLevel = this.heroLevelService.generateHeroLevel();
  }

  getTemplate(heroClass: HeroClass): ClassTemplate {

    let template: ClassTemplate;
    CLASS_TEMPLATES.some(temp => {
      if (temp.heroClass === heroClass) {
        template = temp;
        return true;
      }
    });
    return template;

  }

  buildCharacterEquipment(template: ClassTemplate): CharacterEquipment {
    const equipment = new CharacterEquipment();

    template.equipmentSlotsMap.forEach((value: number, key: EquipmentSlotType) => {
      equipment.addSlots(key, value);
    });

    return equipment;
  }

  gearCharacterEquipment(template: ClassTemplate, equipment: CharacterEquipment) {
    const itemsToBeEquipped: Item[] = this.itemService.getItems(template.equippedItems);
    const itemsEquipped: Item[] = [];

    itemsToBeEquipped.forEach(item => {
      equipment.equipInFreeSlot(item);
    });


  }


}
