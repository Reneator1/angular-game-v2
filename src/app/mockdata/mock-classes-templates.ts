import {HeroClass} from '../classes/characters/hero-class';
import {CharacterStats} from '../classes/characters/charactercontainers/character-stats';
import {EquipmentSlotType} from '../classes/items/equipment/equipment-slot.enum';
import {ClassTemplate} from '../classes/characters/class-template';

export const CLASS_TEMPLATES: ClassTemplate[] = [
  {
    heroClass: HeroClass.HOBO,
    imgPortrait: 'assets/hero2.png',
    characterStats: new CharacterStats(),
    equipmentSlotsMap: new Map<EquipmentSlotType, number>([
      [EquipmentSlotType.WEAPON, 2]]),
    equippedItems: [2, 1],
    inventorySize: 20,
  },
  {
    heroClass: HeroClass.WARRIOR,
    imgPortrait: 'assets/hero1.png',
    characterStats: new CharacterStats(),
    equipmentSlotsMap: new Map<EquipmentSlotType, number>([
      [EquipmentSlotType.WEAPON, 2]]),
    equippedItems: [1, 2],
    inventorySize: 20,
  },
  {
    heroClass: HeroClass.CLERIC,
    imgPortrait: 'assets/hero3.png',
    characterStats: new CharacterStats(),
    equipmentSlotsMap: new Map<EquipmentSlotType, number>([
      [EquipmentSlotType.WEAPON, 2]]),
    equippedItems: [1, 2],
    inventorySize: 20,
  }

  // new Monster('', 500, 10, true, 30, 'WereWolf', 'Water', 'This monster fears water, although it was born in it, molded by it!', 100),
  // new Monster('', 500, 10, true, 45, 'Woof', 'Earth', 'Born from Furfags making love. A scientific impossibility!', 100),
  // new Monster('', 500, 10, true, 50, 'Firedispenser', 'Fire', 'Born from the raging fires of the Underworld.', 100),
];
