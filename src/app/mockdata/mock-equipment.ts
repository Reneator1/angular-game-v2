import {EquipmentSlotType} from '../classes/items/equipment/equipment-slot.enum';
import {EquipmentType} from '../classes/items/equipment/equipment-type.enum';
import {Item} from '../classes/items/Item';

export const ITEM_LIST: Item[] = [

  new Item(1, 'smol Axe', 'assets/i1-axe.png', 2, 5, EquipmentType.WEAPON, EquipmentSlotType.WEAPON),
  new Item(2, 'Big Axe', 'assets/i2-big-axe.png', 5, 5, EquipmentType.WEAPON, EquipmentSlotType.WEAPON),

];
