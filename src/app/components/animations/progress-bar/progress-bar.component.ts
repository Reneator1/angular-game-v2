import {Component, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {timer} from 'rxjs';

@Component({
  selector: 'app-progress-bar',
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        width: '100px',
        height: '100px',
        opacity: 1,
        backgroundColor: 'yellow'
      })),
      state('closed', style({
        width: '0px',
        height: '100px',
        opacity: 1,
        backgroundColor: 'green'
      })),
      transition('open => closed', [
        animate('0s')
      ]),
      transition('closed => open', [
        // animate(this.progressTimer + 's')
      ]),
    ]),
  ],
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.css']
})
export class ProgressBarComponent implements OnInit {
  isOpen = true;

  constructor() {
  }

  ngOnInit() {
  }

  toggle() {
    this.isOpen = !this.isOpen;
  }

  redrawBar(attackDelay: number) {
    //
    // /* Every avatar will move on it's own: */
    // const keyframes = (index: number): AnimationKeyFrame[] => {
    //   return [
    //     /* It starts in a random position: */
    //     {transform: `translate(${random(10, 350)}px, ${random(-10, -550)}px) scale(0)`},
    //     /* ...and ends up in a carefully calculated place: */
    //     {transform: `translate(${(index - 1) * avatarSpace}px, -120px) scale(1)`}
    //   ];
    // };
    //
    // /* Every avatar will move with it's own speed */
    // const options = (): AnimationEffectTiming => ({
    //   duration: random(500, 2000),
    //   easing: 'ease-in-out',
    //   fill: 'forwards'
    // });
    //
    // /* Ok, not let's start moving! */
    // timer(0, 125)                                        // Staggering animation
    //   .pipe(
    //     take(this.people.length)
    //   )
    //   .subscribe(index => {
    //     document.getElementById(`person_${index + 1}`)   // Web Animations API
    //       .animate(keyframes(index + 1), options());     // - not that complex, right?
    //   });
    //
    // this.animate("5s", keyframes([
    //   style({ backgroundColor: "red", offset: 0 }),
    //   style({ backgroundColor: "blue", offset: 0.2 }),
    //   style({ backgroundColor: "orange", offset: 0.3 }),
    //   style({ backgroundColor: "black", offset: 1 })
    // ]))
    //
    // document.getElementById(`person_${index + 1}`).animate(
    //
    //   animate("5s", keyframes([
    //     style({ backgroundColor: "red", offset: 0 }),
    //     style({ backgroundColor: "blue", offset: 0.2 }),
    //     style({ backgroundColor: "orange", offset: 0.3 }),
    //     style({ backgroundColor: "black", offset: 1 })
    //   ]))
    //
    // );
  }
}
