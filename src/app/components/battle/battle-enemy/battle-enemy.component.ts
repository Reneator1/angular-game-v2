import {Component, Input, OnInit} from '@angular/core';
import {Character} from '../../../classes/characters/character';

@Component({
  selector: 'app-battle-enemy',
  templateUrl: './battle-enemy.component.html',
  styleUrls: ['./battle-enemy.component.css']
})
export class BattleEnemyComponent implements OnInit {


  @Input() enemy: Character;
  constructor() { }

  ngOnInit() {
  }

}
