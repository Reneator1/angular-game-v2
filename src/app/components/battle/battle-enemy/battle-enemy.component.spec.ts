import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BattleEnemyComponent } from './battle-enemy.component';

describe('BattleEnemyComponent', () => {
  let component: BattleEnemyComponent;
  let fixture: ComponentFixture<BattleEnemyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BattleEnemyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BattleEnemyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
