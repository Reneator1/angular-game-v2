import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {Hero} from '../../../classes/characters/hero';
import {CharacterEquipment} from '../../../classes/characters/charactercontainers/character-equipment';
import {EventGearChange} from '../../../classes/event/event-gear-change';
import {CombatCycleStatus} from '../../../classes/event/combat-cycle-status.enum';
import {EventCombatCycle} from '../../../classes/event/event-combat-cycle';
import {Character} from '../../../classes/characters/character';
import {Bounce, Power1, Linear} from 'gsap/all';
import {ProgressBarAnimatedDirective} from '../../../directives/progress-bar-animated.directive';


declare var TweenMax: any;

@Component({
  selector: 'app-battle-hero',
  templateUrl: './battle-hero.component.html',
  styleUrls: ['./battle-hero.component.css']
})
export class BattleHeroComponent implements OnInit {

  color = 'orange';

  startAnimation = false;

  boxClass = true;


  @ViewChild('progressBar') progressBar: ElementRef;
  @ViewChild('mushroom') box: ElementRef;
  @Input() hero: Hero;

  constructor() {
    CharacterEquipment.onGearChange.subscribe((event: EventGearChange) => {
      if (event.cause instanceof Hero) {
        // this.setAttackSpeed();
      }
    });
    Character.onCombatCycle.subscribe((event: EventCombatCycle) => {
      if (!(event.cause === this.hero)) {
        console.log('Event cause is not like hero: cause: ' + event.cause + ' hero: ' + this.hero);
      }
      this.doIt();
      switch (event.cycle) {

        case CombatCycleStatus.START:
          this.startAnimation = true;
          // document.getElementsByClassName('attack-bar')[0].style.animation = `do ${this.hero.attackSpeed / 1000}s linear`
          console.log('Animation started!');
          break;

        case CombatCycleStatus.END:
          this.startAnimation = false;
          // document.getElementsByClassName('attack-bar')[0].style.animation = ``;

          console.log('Animation stopped!');

          break;

      }

    });


  }

  ngOnInit() {
    // this.doIt();
  }

  doIt(): void {
    TweenMax.fromTo(this.progressBar.nativeElement, this.hero.attackSpeed / 1000, {width: 0}, {width: 100, ease: Linear.easeNone});
    // TweenMax.fromTo(this.box.nativeElement, 2, {x: 20}, {x: 440, ease: Power1.easeOut});
    // TweenMax.fromTo(this.box.nativeElement, 2, {y: 20}, {y: 440, ease: Bounce.easeOut});
  }

  setAttackSpeed() {
    // this.progressBar.style.animation = `do ${this.hero.attackSpeed} infinite linear`;
    // $('.attack-bar').css('animation', `do ${this.hero.attackSpeed} infinite linear`);
  }

}
