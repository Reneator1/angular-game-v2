import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BattleHeroComponent } from './battle-hero.component';

describe('BattleHeroComponent', () => {
  let component: BattleHeroComponent;
  let fixture: ComponentFixture<BattleHeroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BattleHeroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BattleHeroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
