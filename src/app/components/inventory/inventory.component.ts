import {Component, OnInit} from '@angular/core';
import {PlayerWindow} from '../playerwindows';
import {ItemSlot} from '../../classes/items/equipment/item-slot';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent extends PlayerWindow implements OnInit {


  imgEmptySlot =  'assets/inventory-empty.png';

  constructor() {
    super();
  }

  ngOnInit() {
    console.log(this.hero.inventory);
  }

  clickItem(inventorySlot: ItemSlot) {
    if (inventorySlot == null) {
      console.log('Null Inventory-Slot');
    } else {
      if (inventorySlot.isEmpty()) {
        console.log('You clicked an empty Inventor-slot!');
      } else {
        console.log('An Inventory-Slot has been clickd with item: ' + inventorySlot.item.name + ' inside.');
      }
    }
  }
}
