import {Component, OnInit} from '@angular/core';
import {HeroClass} from '../../classes/characters/hero-class';
import {PlayerWindow} from '../playerwindows';
import {HeroService} from '../../services/hero.service';
import {HeroLevelService} from '../../services/herolevel.service';
import {ItemService} from '../../services/item.service';

@Component({
  selector: 'app-herocreator',
  templateUrl: './herocreator.component.html',
  styleUrls: ['./herocreator.component.css'],
  providers: [HeroService, HeroLevelService, ItemService]
})
export class HerocreatorComponent extends PlayerWindow implements OnInit {

  heroClassOptions: string[] = Object.keys(HeroClass);
  heroName: string;
  heroClass: HeroClass;

  constructor(private heroService: HeroService) {
    super();
  }

  ngOnInit() {
  }

  createCharacter() {
    this.heroService.generateHero(this.hero, this.heroName, this.heroClass);

    this.active.windowCharacterChreation = false;
    this.active.windowOverview = true;
  }

}
