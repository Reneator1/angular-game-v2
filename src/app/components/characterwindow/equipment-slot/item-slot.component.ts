import {Component, Input, OnInit} from '@angular/core';
import {DndDropEvent} from 'ngx-drag-drop';
import {ItemSlot} from '../../../classes/items/equipment/item-slot';
import {SlotType} from '../../../classes/items/equipment/slot.enum';
import {Hero} from '../../../classes/characters/hero';

@Component({
  selector: 'app-equipment-slot',
  templateUrl: './item-slot.component.html',
  styleUrls: ['./item-slot.component.css']
})
export class ItemSlotComponent implements OnInit {

  @Input() itemSlot: ItemSlot;
  @Input() hero: Hero;
  @Input() imgEmptySlot: string;
  draggable = {
    data: this.itemSlot,
    effectAllowed: 'all',
    disable: false,
    handle: false,
  };

  constructor() {
  }

  ngOnInit() {
    console.log(this.imgEmptySlot);
  }

  click(equipmentSlot: ItemSlot) {
    if (equipmentSlot == null) {
      console.log('Null as EquipmentSlot');
    } else {
      if (equipmentSlot.isEmpty()) {
        console.log('You clicked the empty equipment-Slot: ' + equipmentSlot.equipmentSlotType);
      } else {
        console.log('You clicked the equipment-Slot: ' + equipmentSlot.equipmentSlotType +
          ' with the item: ' + equipmentSlot.item.name + ' inside!');
      }
    }

  }

  onDrop(event: DndDropEvent) {

    const slot: ItemSlot = event.data;
    if (slot.id === this.itemSlot.id) {
      return;
    }

    let originSlot: ItemSlot;
    if (slot.slotType === SlotType.INVENTORY) {
      originSlot = this.hero.inventory.getById(slot.id);
    }
    if (slot.slotType === SlotType.EQUIPMENT) {
      originSlot = this.hero.equipment.getById(slot.id);
    }

    if (originSlot == null) {
      console.error('Es konnte kein ItemSlot für die Id ' + slot.id + ' gefunden werden');
      return;
    }

    // originSlot.exchange(this.equipmentSlot);


    // if(){
    //
    // }
    // this.characterInventory.getById();

    this.itemSlot.exchangeItem(originSlot);

    console.log('dropped onto: ' + JSON.stringify(this.itemSlot), JSON.stringify(event, null, 2));
  }

}
