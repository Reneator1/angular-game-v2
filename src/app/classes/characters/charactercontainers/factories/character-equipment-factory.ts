import {CharacterEquipment} from '../character-equipment';
import {EquipmentSlotType} from '../../../items/equipment/equipment-slot.enum';

export class CharacterEquipmentFactory {

  makeCharacterEquipmentSlots(slotMap: Map<EquipmentSlotType, number>) {
    const characterEquipment = new CharacterEquipment();

    slotMap.forEach((value: number, key: EquipmentSlotType) => {
        while (value >= 1) {
          characterEquipment.addSlot(key);
          value--;
        }
      }
    );
  }

  makeCharacterEquipment(equipmentIds: number[]) {
    equipmentIds.forEach(id => {

    });
  }

}
