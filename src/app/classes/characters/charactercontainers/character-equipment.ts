import {EquipmentSlotType} from '../../items/equipment/equipment-slot.enum';
import {ItemSlot} from '../../items/equipment/item-slot';
import {Item} from '../../items/Item';
import {EventEmitter} from '@angular/core';
import {EventGearChange} from '../../event/event-gear-change';
import {SlotType} from '../../items/equipment/slot.enum';

export class CharacterEquipment {
  public static onGearChange = new EventEmitter<EventGearChange>();


  private slots: ItemSlot[] = [];


  getSlots(equipmentSlotType: EquipmentSlotType): ItemSlot[] {
    if (equipmentSlotType == null) {
      return this.slots;
    }
    const slots = [];

    this.slots.forEach(slot => {
      if (slot.equipmentSlotType === equipmentSlotType) {
        slots.push(slot);
      }
    });

    return slots;
  }


  addSlot(equipmentSlotType: EquipmentSlotType) {
    this.slots.push(new ItemSlot(SlotType.EQUIPMENT, equipmentSlotType));
  }

  addSlots(equipmentSlotType: EquipmentSlotType, count: number) {
    while (count >= 1) {
      this.slots.push(new ItemSlot(SlotType.EQUIPMENT, equipmentSlotType));
      count--;
    }
  }

  addEquipmentSlot(slot: ItemSlot) {
    this.slots.push(slot);
  }

  public getById(id: number): ItemSlot {
    if (id == null) {
      console.error('The id is null');
    }

    let returnSlot: ItemSlot;
    this.slots.forEach(slot => {
      if (slot.id === id) {
        returnSlot = slot;
      }
    });

    return returnSlot;

  }

  equipInFreeSlot(item: Item) {
    const slots: ItemSlot[] = this.getSlots(item.equipmentSlotType);

    slots.forEach(slot => {
      if (slot.isEmpty()) {
        slot.exchange(item);
      }
    });
  }
}
