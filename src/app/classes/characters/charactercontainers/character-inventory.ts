import {Item} from '../../items/Item';
import {ItemSlot} from '../../items/equipment/item-slot';
import {SlotType} from '../../items/equipment/slot.enum';

export class CharacterInventory {

  private inventorySlots: ItemSlot[] = [];


  constructor(size: number) {
    for (let i = 0; i <= size - 1; i++) {
      this.inventorySlots.push(new ItemSlot(SlotType.INVENTORY, null));
    }
  }

  getSlots() {
    return this.inventorySlots;
  }

  // getEmptySlots() {
  //   this.inventorySlots.forEach( slot => )
  //
  //   return this.size - this.inventorySlots.length;
  // }

  addItem(item: Item): boolean {
    const added = false;
    return added || this.inventorySlots.some(
      slot => {
        if (slot.isEmpty()) {
          slot.exchange(item);
          return true;
        }
      });
  }

  getEmptySlot() {

  }

  public getById(id: number): ItemSlot {
    if (id == null) {
      console.error('The id is null');
    }
    let returnSlot: ItemSlot;
    this.inventorySlots.forEach(slot => {
      if (slot.id === id) {
        returnSlot = slot;
      }
    });

    return returnSlot;
  }

  public size(): number {
    return this.inventorySlots.length;
  }

}
