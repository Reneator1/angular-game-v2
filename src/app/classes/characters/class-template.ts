import {EquipmentSlotType} from '../items/equipment/equipment-slot.enum';
import {CharacterStats} from './charactercontainers/character-stats';
import {HeroClass} from './hero-class';

export class ClassTemplate {

  heroClass: HeroClass;
  imgPortrait: string;
  equipmentSlotsMap: Map<EquipmentSlotType, number>;
  characterStats: CharacterStats;
  equippedItems: number[];
  inventorySize: number;

}
