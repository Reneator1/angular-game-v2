import {MatTooltipDefaultOptions} from '@angular/material';

export const TooltipDefaults: MatTooltipDefaultOptions = {
  showDelay: 1000,
  hideDelay: 1000,
  touchendHideDelay: 1000
};
