import {GameObject} from '../game-object';
import {EquipmentType} from './equipment/equipment-type.enum';
import {EquipmentSlotType} from './equipment/equipment-slot.enum';

export class Item extends GameObject {
  goldValue: number;
  itemId: number;
  name: string;
  type: EquipmentType;
  equipmentSlotType: EquipmentSlotType;
  value: number;


  constructor(itemId: number, name: string, img: string, value: number,
              goldValue: number, type: EquipmentType, slotType: EquipmentSlotType) {
    super();
    this.type = type;
    this.equipmentSlotType = slotType;
    this.value = value;
    this.goldValue = goldValue;
    this.itemId = itemId;
    this.name = name;
    this.img = img;
  }

  clone(): Item {
    return new Item(this.itemId, this.name, this.img, this.value, this.goldValue, this.type, this.equipmentSlotType);
  }
}


