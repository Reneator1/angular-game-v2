import {CombatCycleStatus} from './combat-cycle-status.enum';
import {Character} from '../characters/character';

export class EventCombatCycle {

  cause: Character;
  cycle: CombatCycleStatus;


  constructor(cause: Character, cycle: CombatCycleStatus) {
    this.cause = cause;
    this.cycle = cycle;
  }
}
